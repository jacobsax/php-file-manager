PHP-File-Manager
================

A php file manager to be used by users of an internet site to view and manipulate files in a defined area

Features:
* Navigate through directory
* Download files
* Rename files
* Delete files

Still to do:
* Upload (user will upload as a zip file, wich will then be extracted)
* Download (user will download folders as .zip files, normal file downloads already work)
* View file (show file in browser based viewer)
* Create directory
* Delete directory
* Ability to view directories outside of main directory where files are held

Possible:
* Better graphics (the current version has incredibly 'minimal' text based graphics, I want to create a version which will use much better graphics and a more computer like user interface)
